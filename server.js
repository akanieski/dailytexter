//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');

var async = require('async');
var fs = require('fs');
var socketio = require('socket.io');
var express = require('express');
var cheerio = require('cheerio');
var request = require('request');
var moment = require('moment');
var schedule = require('node-schedule');
var emailjs = require('emailjs');
var email  = emailjs.server.connect({
   user:     "andrewk@varsityg.com", 
   password: "Shamoo00#", 
   host:     "smtp.gmail.com", 
   ssl:      true
});

//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));

String.prototype.replaceAll = function(character,replaceChar){
    var word = this.valueOf();

    while(word.indexOf(character) != -1)
        word = word.replace(character,replaceChar);

    return word;
}

function send_text() {
  request('http://wol.jw.org/en/wol/dt/r1/lp-e/'+moment().format('YYYY')+'/'+moment().format('MM')+'/'+moment().format('DD') , function(x,y,body){
    
    var txt = cheerio.load(body)('.scalableui').html().replaceAll('href="/en/', 'href="http://wol.jw.org/en/');
    var list = fs.readFileSync('./mailinglist.txt').toString();
    
    console.log(list);
    
    var message = {
       text:    "Daily Text", 
       from:    "Daily Text Sender <andrew.legacy@gmail.com>", 
       bcc:     list.replace('\r\n', ', '),
       subject: "Daily Text - " + moment().format('dddd, MMMM Do YYYY'),
       attachment: 
       [
          {data: txt, alternative:true}
       ]
    };
    
    // send the message and get a callback with an error or details of the message that was sent
    email.send(message, function(err, message) { console.log(err || message); });
    
  })
}

schedule.scheduleJob({hour: 8}, function(){
//  send_text();
});


router.get('/api/send', function(req, res, next){

  send_text();
  
  res.send(200);
  
});

var messages = [];
var sockets = [];

io.on('connection', function (socket) {
    messages.forEach(function (data) {
      socket.emit('message', data);
    });

    sockets.push(socket);

    socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      updateRoster();
    });

    socket.on('message', function (msg) {
      var text = String(msg || '');

      if (!text)
        return;

      socket.get('name', function (err, name) {
        var data = {
          name: name,
          text: text
        };

        broadcast('message', data);
        messages.push(data);
      });
    });

    socket.on('identify', function (name) {
      socket.set('name', String(name || 'Anonymous'), function (err) {
        updateRoster();
      });
    });
  });

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      broadcast('roster', names);
    }
  );
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});


